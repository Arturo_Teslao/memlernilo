/*
  Aŭtorrajto © 2023-2025 Arturo Teslao
  Ĉi tiu verko estas disponebla laŭ la permesilo AGPL.

  Copyright © 2023-2025 Arthur Tesla
  This work is licensed under a GNU AGPL.




  # apt install libsqlite3-dev
  $ gcc -g -Wall main.c -o test -lsqlite3
*/
/* --------------------------------------------------------- */
#include <stdio.h>    /* Por enigo-elsendo */
#include <stdlib.h>   /* Por memoro-asignado kaj procezo-administrado */
#include <sqlite3.h>  /* Por labori kun SQLite-datumbazo */
#include <string.h>   /* Por labori kun ĉenoj */
/* --------------------------------------------------------- */
char my_words_user[64];  /*  */
char my_words_esperanto[64];  /*  */
char my_words_english[64];  /*  */
#if 0
unsigned char *audio_data;  /* sonoj */
int audio_size;  /* sonoj */
unsigned char *image_data;  /* bildoj */
int image_size;  /* bildoj */
#endif
/* --------------------------------------------------------- */
void greetings()
  {
    puts("Saluton!");
    puts("");
  }
/* --------------------------------------------------------- */
void read_random_words()
  {
    sqlite3 *db;
    sqlite3_stmt *stmt;
    int rc;

    rc = sqlite3_open_v2("Memlernilo.sqlite3", &db, SQLITE_OPEN_READONLY, NULL);
    if(rc != SQLITE_OK)
      {
        fprintf(stderr, "Ne povas malfermi datumbazon %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        exit(1);
      }

    const char *sql = "SELECT Vorto, Vorto_angla FROM Vortoj JOIN Vortoj_anglaj ON Vortoj.id = Vortoj_anglaj.id ORDER BY RANDOM() LIMIT 1;";

    rc = sqlite3_prepare_v2(db, sql, -1, &stmt, NULL);
    if(rc != SQLITE_OK)
      {
        fprintf(stderr, "Eraro en SQL: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        exit(1);
      }

    rc = sqlite3_step(stmt);
    if(rc == SQLITE_ROW)
      {
        const unsigned char *word_esperanto = sqlite3_column_text(stmt, 0);
        const unsigned char *word_english = sqlite3_column_text(stmt, 1);
        if(strlen((char*)word_esperanto) >= sizeof(my_words_esperanto))
          {
            sqlite3_finalize(stmt);
            sqlite3_close(db);
            exit(1);
          }
        if(strlen((char*)word_english) >= sizeof(my_words_english))
          {
            sqlite3_finalize(stmt);
            sqlite3_close(db);
            exit(1);
          }
        strcpy(my_words_esperanto, (char*)word_esperanto);
        strcpy(my_words_english, (char*)word_english);
#if 0
        printf("Por testado: hazarda vorto en la angla lingvo %s\n", my_words_esperanto);
        printf("Por testado: hazarda vorto en la Esperanto lingvo %s\n", my_words_english);
#endif
      }
    else
      {
        printf("Ne estas datumoj en la tabelo de Vortoj aŭ Vortoj_angla\n");
      }

    sqlite3_finalize(stmt);
    sqlite3_close(db);
  }
/* --------------------------------------------------------- */
int read_user_words()
  {
    while(1)
      {
        read_random_words();
        while(1)
          {
            printf("Enigu tradukon por la frazo: %s\n", my_words_esperanto);
            printf("Cia respondo: ");
            scanf("%s", my_words_user);

            if(strcmp(my_words_user, my_words_english) == 0)
              {
                puts("Ĝuste!");
                puts("");
                break;
              }
            else
              {
                puts("Malĝusta respondo, provu denove!");
                puts("");
              }
          }
      }
    return 0;
  }
/* --------------------------------------------------------- */
void my_byebye()
  {
    puts("");
    puts("Ĝis!");
  }
/* --------------------------------------------------------- */
int main()
  {
    greetings();
#if 0
    read_random_words();
#endif
    read_user_words();
    my_byebye();
    return 0;
  }
/* --------------------------------------------------------- */
